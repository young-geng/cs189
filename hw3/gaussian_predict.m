function label = gaussian_predict(m, c,  X)
    p = [];
    for idx = [1:10]
        p = [p, mvnpdf(X, m(idx, :), c(:, :, idx))];
    end
    label = [];
    for idx = [1:size(X, 1)]
        l = find(p(idx, :) == max(p(idx, :)));
        if l == 10
            l = 0;
        end
        label = [label; l];
    end
end