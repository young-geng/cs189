clear;

load 'data/spam-dataset/spam_data.mat';
train_x = double(training_data);
train_y = double(training_labels)';
train_size = size(train_x, 1);
test_x = double(test_data);
test_size = size(test_data, 1);
feature_size = size(train_x, 2);

% Randomize data
rand_order = randperm(train_size);
train_x = train_x(rand_order, :);
train_y = train_y(rand_order, :);

% Training for kaggle
fprintf('Now computing kaggle data\n');




indices = train_y == 0;
x = train_x(indices, :);
ham_mean = mean(x, 1);
ham_cov = ((x - repmat(ham_mean, size(x, 1), 1))' * (x - repmat(ham_mean, size(x, 1), 1))) ./ size(x, 1);
while det(ham_cov) == 0
    ham_cov = ham_cov + 0.1 * eye(feature_size);
end



indices = train_y == 1;
x = train_x(indices, :);
spam_mean = mean(x, 1);
spam_cov = ((x - repmat(spam_mean, size(x, 1), 1))' * (x - repmat(spam_mean, size(x, 1), 1))) ./ size(x, 1);
while det(spam_cov) == 0
    spam_cov = spam_cov + 0.1 * eye(feature_size);
end

p_ham = sum(train_y == 0) / train_size;
p_spam = sum(train_y == 1) / train_size;

fprintf('p_spam: %f, p_ham: %f\n', p_spam, p_ham);

% Compute posterior probability for spam class
pc_spam = mvnpdf(test_x, spam_mean, spam_cov) * p_spam ./ (p_spam * mvnpdf(test_x, spam_mean, spam_cov) + p_ham * mvnpdf(test_x, ham_mean, ham_cov));
labels = pc_spam > 0.5;

disp(sum(labels));


indices = [1:test_size]';
predicted_y = [indices, labels];

FILE = fopen('kaggle_spam.csv', 'w');
fprintf(FILE, 'Id,Category\n');
fclose(FILE);

dlmwrite('kaggle_spam.csv', predicted_y, '-append');
