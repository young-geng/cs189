clear;

% Load data and reshape it into vectors
load 'data/digit-dataset/train.mat'
train_size = size(train_label, 1);
train_x = reshape(train_image, size(train_image, 1) * size(train_image, 2), size(train_image, 3))';
train_y = train_label;
feature_size = size(train_x, 2);

for idx = [1:train_size]
    train_x(idx, :) = train_x(idx, :) ./ norm(train_x(idx, :));
end

rand_order = randperm(train_size);
train_x = train_x(rand_order, :);
train_y = train_y(rand_order, :);

load('data/digit-dataset/test.mat');
test_size = size(test_image, 3);
test_x = reshape(test_image, size(test_image, 1) * size(test_image, 2), size(test_image, 3))';
test_y = test_label;

for idx = [1:test_size]
    test_x(idx, :) = test_x(idx, :) ./ norm(test_x(idx, :));
end

mean_vector = zeros(10, feature_size);
cov_matrix = zeros(feature_size, feature_size, 10);


% Estimate gaussian parameters
fprintf('Estimating gaussian parameters\n')
for idx = [1:10]
    if idx == 10
        indices = train_y == 0;
    else
        indices = train_y == idx;
    end
    x = train_x(indices, :);
    mean_vector(idx, :) = mean(x);
    cov_matrix(:, :, idx) = ((x - repmat(mean_vector(idx, :), size(x, 1), 1))' * (x - repmat(mean_vector(idx, :), size(x, 1), 1))) ./ size(x, 1);
end



% Plot covariance covariance matrix
fprintf('Ploting covariance matrix\n')
for idx = [1:10]
    subplot(2, 5, idx);
    imagesc(cov_matrix(:, :, idx));
    if idx == 10
        title(sprintf('Class: %d', 0));
    else
        title(sprintf('Class: %d', idx));
    end
end
disp('Press any key to continue');
pause;



% Classify test data
fprintf('Training gaussian classifier with mean covariance\n')
mean_vector = zeros(10, feature_size);
cov_matrix = zeros(feature_size, feature_size, 10);
sample_size = [100, 200, 500, 1000, 2000, 5000, 10000, 30000, 60000];
error_rate = [];
for s = sample_size
    fprintf('       training with sample size %d\n', s);
    temp_x = train_x(1:s, :);
    temp_y = train_y(1:s, :);
    for idx = [1:10]
        if idx == 10
            indices = temp_y == 0;
        else
            indices = temp_y == idx;
        end
        x = temp_x(indices, :);
        mean_vector(idx, :) = mean(x);
        cov_matrix(:, :, idx) = ((x - repmat(mean_vector(idx, :), size(x, 1), 1))' * (x - repmat(mean_vector(idx, :), size(x, 1), 1))) ./ size(x, 1);
    end
    c = mean(cov_matrix, 3);
    while det(c) == 0
        c = c + 0.1 * eye(feature_size);
    end
    for idx = [1:10]
        cov_matrix(:, :, idx) = c;
    end
    predicted_y = gaussian_predict(mean_vector, cov_matrix, test_x);
    error_rate = [error_rate, sum(predicted_y ~= test_y) / test_size];
end
semilogx(sample_size, error_rate);
disp('Press any key to continue');
pause;


fprintf('Training gaussian classifier with individual covariance\n');
mean_vector = zeros(10, feature_size);
cov_matrix = zeros(feature_size, feature_size, 10);
error_rate = [];
for s = sample_size
    fprintf('       training with sample size %d\n', s);
    temp_x = train_x(1:s, :);
    temp_y = train_y(1:s, :);
    for idx = [1:10]
        if idx == 10
            indices = temp_y == 0;
        else
            indices = temp_y == idx;
        end
        x = temp_x(indices, :);
        mean_vector(idx, :) = mean(x);
        cov_matrix(:, :, idx) = ((x - repmat(mean_vector(idx, :), size(x, 1), 1))' * (x - repmat(mean_vector(idx, :), size(x, 1), 1))) ./ size(x, 1);
    end
    for idx = [1:10]
        while det(cov_matrix(:, :, idx)) == 0
            cov_matrix(:, :, idx) = cov_matrix(:, :, idx) + 0.1 * eye(feature_size);
        end
    end
    predicted_y = gaussian_predict(mean_vector, cov_matrix, test_x);
    error_rate = [error_rate, sum(predicted_y ~= test_y) / test_size];
end
semilogx(sample_size, error_rate);
disp('Press any key to continue');
pause;




% Training for kaggle
fprintf('Now computing kaggle data\n');
mean_vector = zeros(10, feature_size);
cov_matrix = zeros(feature_size, feature_size, 10);
for idx = [1:10]
    if idx == 10
        indices = train_x == 0;
    else
        indices = train_y == idx;
    end
    x = train_x(indices, :);
    mean_vector(idx, :) = mean(x);
    cov_matrix(:, :, idx) = ((x - repmat(mean_vector(idx, :), size(x, 1), 1))' * (x - repmat(mean_vector(idx, :), size(x, 1), 1))) ./ size(x, 1);
end
c = mean(cov_matrix, 3);
while det(c) == 0
    c = c + 0.1 * eye(feature_size);
end
for idx = [1:10]
    cov_matrix(:, :, idx) = c;
end

load 'data/digit-dataset/kaggle.mat'
kaggle_x = reshape(kaggle_image, size(kaggle_image, 1) * size(kaggle_image, 2), size(kaggle_image, 3))';
kaggle_size = size(kaggle_x, 1);
for idx = [1:kaggle_size]
    kaggle_x(idx, :) = kaggle_x(idx, :) ./ norm(kaggle_x(idx, :));
end
predicted_y = gaussian_predict(mean_vector, cov_matrix, kaggle_x);
indices = [1:kaggle_size]';
predicted_y = [indices, predicted_y];
FILE = fopen('kaggle_digit.csv', 'w');
fprintf(FILE, 'Id,Category\n');
fclose(FILE);

dlmwrite('kaggle_digit.csv', predicted_y, '-append');


