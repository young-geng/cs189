function labels = predict_spam(ham_mean, ham_cov, spam_mean, spam_cov, p_ham, p_spam, X)
    pc_spam = mvnpdf(X, spam_mean, spam_cov) .* p_spam ./ (p_spam .* mvnpdf(X, spam_mean, spam_cov) + p_ham .* mvnpdf(X, ham_mean, ham_cov));
    labels = pc_spam > 0.5
