function [dW1, dW2] = mean_square_gradient(W1, W2, X, Y)
    dW1 = zeros(size(W1));
    dW2 = zeros(size(W2));
    l0 = 28 * 28;
    l1 = 200;
    l2 = 10;
    for idx = 1:size(X, 1)
        y = Y(idx, :)';
        x0 = [1, X(idx, :)]';
        c1 = W1' * x0;
        x1 = [1; tanh(c1)];
        c2 = W2' * x1;
        x2 = sigmf(c2, [1, 0]);

        t = diag(sigmf(c2, [1, 0]) .* (1 - sigmf(c2, [1, 0]))) * (x2 - y); % dL / dc2

        dW2 = dW2 + repmat(x1, 1, l2) * diag(t);

        dW1 = dW1 + repmat(x0, 1, l1) * diag([zeros(l1, 1), diag(1 - tanh(c1) .^ 2)] * W2 * t);

    end


    dW1 = dW1 ./ size(X, 1);
    dW2 = dW2 ./ size(X, 1);

end