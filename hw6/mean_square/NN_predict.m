function y = NN_predict(W1, W2, x)
    % y = [];
    % for idx = 1:size(x, 1)
    %     x0 = [1, x(idx, :)]';
    %     c1 = W1' * x0;
    %     x1 = [1; tanh(c1)];
    %     c2 = W2' * x1;
    %     y = [y; sigmf(c2, [1, 0])'];
    % end

    x0 = [ones(size(x, 1), 1), x]';
    c1 = W1' * x0;
    x1 = [ones(1, size(c1, 2)); tanh(c1)];
    c2 = W2' * x1;
    y = sigmf(c2, [1, 0])';
end