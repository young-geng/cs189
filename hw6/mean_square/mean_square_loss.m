function C = mean_square_loss(W1, W2, X, Y)
    C = 0.5 * sum(sum((NN_predict(W1, W2, X) - Y) .^ 2)) / size(X, 1);
end