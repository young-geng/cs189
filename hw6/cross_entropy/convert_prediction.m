function p = convert_prediction(Y)
    p = [];
    for idx = 1:size(Y, 1)
        p = [p; find(Y(idx, :) == max(Y(idx, :)), 1) - 1];
    end
end