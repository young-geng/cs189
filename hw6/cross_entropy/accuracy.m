function R = accuracy(predicted_Y, real_Y)
    correct = 0;
    for idx = 1:size(predicted_Y, 1)
        if find(predicted_Y(idx, :) == max(predicted_Y(idx, :)), 1)  ==  find(real_Y(idx, :) == max(real_Y(idx, :)), 1)
            correct = correct + 1;
        end
    end
    R = correct / size(predicted_Y, 1);
end