function C = cross_entropy_loss(W1, W2, X, Y)
    predicted_Y = NN_predict(W1, W2, X);
    C = - sum(sum(Y .* log(predicted_Y) + (1 - Y) .* log(1 - predicted_Y))) / size(Y, 1);
end
