function [dW1, dW2] = cross_entropy_numerical_gradient(W1, W2, X, Y)
    dW1 = zeros(size(W1));
    dW2 = zeros(size(W2));

    delta = 1e-4;


    for ii = 1:size(W1, 1)
        for jj = 1:size(W1, 2)
            t1 = W1;
            t2 = W1;
            t1(ii, jj) = t1(ii, jj) + delta;
            t2(ii, jj) = t2(ii, jj) - delta;
            dW1(ii, jj) = (cross_entropy_loss(t1, W2, X, Y) - cross_entropy_loss(t2, W2, X, Y)) / (2 * delta);
        end
    end


    for ii = 1:size(W2, 1)
        for jj = 1:size(W2, 2)
            t1 = W2;
            t2 = W2;
            t1(ii, jj) = t1(ii, jj) + delta;
            t2(ii, jj) = t2(ii, jj) - delta;
            dW2(ii, jj) = (cross_entropy_loss(W1, t1, X, Y) - cross_entropy_loss(W1, t2, X, Y)) / (2 * delta);
        end
    end

end