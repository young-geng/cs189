% Load data
close all;
clear
load '../digit-dataset/train.mat'
load '../digit-dataset/test.mat'




% Reshape data
train_images = reshape(train_images, size(train_images, 1) * size(train_images, 2), size(train_images, 3))';
train_labels = train_labels;
temp = [];
for idx = 0:9
    temp = [temp, train_labels == idx];
end
train_labels = temp;
clear temp;
test_images = reshape(test_images, size(test_images, 1) * size(test_images, 2), size(test_images, 3))';

%Shuffle data
rand_order = randperm(size(train_images, 1));
train_images = train_images(rand_order, :);
train_labels = train_labels(rand_order, :);
clear rand_order;



% Choosing training and validation set
train_size = 40000;
train_X = train_images(1:train_size, :);
train_Y = train_labels(1:train_size, :);

validation_size = size(train_images, 1) - train_size;
validation_X = train_images(train_size + 1:end, :);
validation_Y = train_labels(train_size + 1:end, :);



% 2 layer NN
l0 = 28 * 28;
l1 = 200;
l2 = 10;
W1 = (rand(l0 + 1, l1) - 0.5) * 0.1 * 10;
W2 = (rand(l1 + 1, l2) - 0.5) * 0.1 * 10;


load 'long_weight.mat'


iterations = 100000;
if exist('iteration')
    iteration_start = iteration;
else
    iteration_start = 1;
end
loss = [];




iterations = 100000;
iteration_start = 1;
loss = [];

step_size_modifier = 0.19;
decrease_count = 0;
last_loss = 999;


for iteration = [iteration_start:iteration_start + iterations]

    random_indices = randsample(1:size(train_X), 2000);
    random_X = train_X(random_indices, :);
    random_Y = train_Y(random_indices, :);

    [dW1, dW2] = cross_entropy_gradient(W1, W2, random_X, random_Y);
    %[ndW1, ndW2] = cross_entropy_numerical_gradient(W1, W2, train_X, train_Y);

    step_size = 1 / exp(iteration .^ step_size_modifier);

    if mod(iteration, 100) == 0
        predicted_Y = NN_predict(W1, W2, validation_X);
        validation_accuracy = accuracy(predicted_Y, validation_Y);
        train_loss = cross_entropy_loss(W1, W2, train_X, train_Y);
        fprintf('Iteration:  %d   Step:  %f   Step modifier:  %f\nTraining loss:  %f  Validation accuracy:  %f\n\n', iteration, step_size, step_size_modifier, train_loss, validation_accuracy);

        if train_loss >= last_loss
            decrease_count = 0;
            step_size_modifier = step_size_modifier + 0.01;
        elseif decrease_count >= 5
            step_size_modifier = max(step_size_modifier - 0.01, 0.01);
            decrease_count = 0;
        else
            decrease_count = decrease_count + 1;
        end
        last_loss = train_loss; 
    end


    W1 = W1 - step_size * dW1;
    W2 = W2 - step_size * dW2;

end



predicted_Y = NN_predict(W1, W2, validation_X);
validation_accuracy = accuracy(predicted_Y, validation_Y);
disp(validation_accuracy);

test_Y = convert_prediction(NN_predict(W1, W2, test_images));

save('long_weight.mat', 'W1', 'W2', 'iteration');
save('kaggle.mat', 'test_Y'); 

indices = [1:length(test_Y)]';

kaggle_file = fopen('kaggle.csv', 'w');
fprintf(kaggle_file, 'Id,Category\n');
fclose(kaggle_file);
dlmwrite('kaggle.csv', [indices, test_Y], '-append');

