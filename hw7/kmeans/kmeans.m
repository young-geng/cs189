% K means clustering for digit
close all;
clear;

% Load data and reshape it into vectors
load '../mnist_data/images.mat'
train_size = size(images, 3);
train_X = reshape(images, size(images, 1) * size(images, 2), size(images, 3))';
feature_size = size(train_X, 2);
rand_order = randperm(train_size);
train_X = train_X(rand_order, :);


clusters = kmeans_cluster(train_X, 10);

%colorbar;
%for idx = 1:10
%    subplot(2, 5, idx);
%    imagesc(reshape(clusters(idx, :), 28, 28));
%end
