function [clusters] = kmeans_cluster(X, num_clusters)
    MU = mean(X);
    SIGMA = cov(X);
    clusters = mvnrnd(MU, SIGMA, num_clusters);

    last = zeros(1, size(X, 1));
    for iter = 1:1000
        [temp, labels] = pdist2(clusters, X, 'euclidean', 'Smallest', 1);
        num_different = sum(labels ~= last);
        if num_different == 0
            break;
        end
        last = labels;
        loss = 0;
        for idx = 1:num_clusters
            indices = labels == idx;
            clusters(idx, :) = mean(X(indices, :));
            loss = loss + sum(pdist2(X(indices, :), clusters(idx, :)));
        end
        loss = loss / size(X, 1);
        fprintf('Iteration: %d,  Last label changed: %d,  Loss: %f\n', iter, num_different, loss);
    end
end