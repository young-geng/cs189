% Evaluate PCA with different dimension
clear;
close all;

load '../joke_data/joke_train.mat';
validation_data = csvread('../joke_data/validation.txt');
test_data = csvread('../joke_data/query.txt');

num_people = size(train, 1);
num_jokes = size(train, 2);


nan_cordinates = isnan(train);
train(nan_cordinates) = 0;
dimensions_to_test = [2, 5, 10, 20];
loss = [];
for d = dimensions_to_test

    U = randn(num_people, d);
    J = randn(num_jokes, d);


    lambda = 0.001;
    for iter = 1:40
        t = U * J';
        X(nan_cordinates) = t(nan_cordinates);
        U = (train * J - 2 * lambda * U) * pinv(J' * J);
        t = U * J';
        X(nan_cordinates) = t(nan_cordinates);
        J = (train' * U - 2 * lambda * J) * pinv(U' * U);

        % loss = [loss, sqrt(sum(sum((U * J' - train) .^ 2)))];
    end
    loss = [loss, norm(U * J' - train, 'fro') / (num_people * num_jokes)];
    % plot([1:length(loss)], loss);

    predicted = U * J';

    incorrect = 0;
    for idx = 1:size(validation_data, 1)
        user_idx = validation_data(idx, 1);
        joke_idx = validation_data(idx, 2);
        if (predicted(user_idx, joke_idx) > 0) ~= validation_data(idx, 3)
            incorrect = incorrect + 1;
        end
    end

    fprintf('Dimension:  %d  Accuracy: %f\n', d, 1 - incorrect / size(validation_data, 1));
end

plot(dimensions_to_test, loss);


% kaggle_file = fopen('kaggle.csv', 'w');
% fprintf(kaggle_file, 'Id,Category\n');
% for idx = 1:size(test_data, 1)
%     user_idx = test_data(idx, 2);
%     joke_idx = test_data(idx, 3);
%     prediction = predicted(user_idx, joke_idx) > 0;
%     fprintf(kaggle_file, '%d,%d\n', idx, prediction);
% end
% fclose(kaggle_file);