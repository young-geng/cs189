% Average rating recommender system
clear;
close all;

load '../joke_data/joke_train.mat';

num_people = size(train, 1);
num_jokes = size(train, 2);

train(isnan(train)) = 0;
mean_rating = mean(train);
validation_data = csvread('../joke_data/validation.txt');
incorrect = 0;
K = 1000;
for idx = 1:size(validation_data, 1)
    if mod(idx, 10) == 0
        fprintf('User:   %d / %d\n', idx, size(validation_data, 1));
    end
    user_idx = validation_data(idx, 1);
    joke_idx = validation_data(idx, 2);
    [temp, I] = pdist2(train, train(user_idx, :), 'cosine', 'Smallest', K + 1);
    knn_ratings = train(I, :);
    mean_rating = mean(knn_ratings(:, joke_idx));
    if (mean_rating > 0) ~= validation_data(idx, 3)
        incorrect = incorrect + 1;
    end
end

fprintf('Error rate: %f\n', incorrect / size(validation_data, 1));