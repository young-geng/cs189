% Perform PCA
function [projected_data, mean_vector, projection_matrix, recover_matrix] = PCA(X, num_dimensions)
    mean_vector = mean(X);
    X = X - repmat(mean_vector, size(X, 1), 1);
    % [U, S, W] = svd(X, 0);
    % projection_matrix = W';
    % projection_matrix = projection_matrix(1:num_dimensions, :);
    [U, D] = eigs(X' * X / size(X, 1), num_dimensions);
    U = U ./ repmat(sqrt(sum(U .^ 2)), size(U, 1), 1);
    projection_matrix = U;
    recover_matrix = projection_matrix';
    projected_data = X * U;
end

