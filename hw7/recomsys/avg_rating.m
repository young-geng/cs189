% Average rating recommender system
clear;
close all;

load '../joke_data/joke_train.mat';

num_people = size(train, 1);
num_jokes = size(train, 2);

train(isnan(train)) = 0;
mean_rating = mean(train);
validation_data = csvread('../joke_data/validation.txt');
incorrect = 0;
for idx = 1:size(validation_data, 1)
    if (mean_rating(validation_data(idx, 2)) > 0) ~= validation_data(idx, 3)
        incorrect = incorrect + 1;
    end
end

fprintf('Error rate: %f\n', incorrect / size(validation_data, 1));