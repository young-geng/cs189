% Evaluate PCA with different dimension
clear;
close all;

load '../joke_data/joke_train.mat';
validation_data = csvread('../joke_data/validation.txt');

num_people = size(train, 1);
num_jokes = size(train, 2);

train(isnan(train)) = 0;


%Learn representation for user
square_error = [];
dimensions_to_test = [2, 5, 10, 20, 40];
for num_dimensions = dimensions_to_test
    % user_rep = PCA(train, num_dimensions);
    % joke_rep = PCA(train', num_dimensions);
    mean_vector = mean(train);
    X = train - repmat(mean_vector, size(train, 1), 1);
    [U, Sigma, W] = svd(X, 0);
    % user_rep = X * W(:, 1:num_dimensions);
    % joke_rep = X' * U(:, 1:num_dimensions);
    user_rep = U(:, 1:num_dimensions) * sqrt(Sigma(1:num_dimensions, 1:num_dimensions));
    joke_rep = W(:, 1:num_dimensions) * sqrt(Sigma(1:num_dimensions, 1:num_dimensions));
    predicted_ratings = user_rep * joke_rep' + repmat(mean_vector, size(X, 1), 1);
    square_error = [square_error, norm(train - predicted_ratings, 'fro') / (num_jokes * num_people)];
    incorrect = 0;
    for idx = 1:size(validation_data, 1)
        user_idx = validation_data(idx, 1);
        joke_idx = validation_data(idx, 2);
        if (predicted_ratings(user_idx, joke_idx) > 0) ~= validation_data(idx, 3)
            incorrect = incorrect + 1;
        end
    end
    fprintf('Dimension:   %d   Accuracy: %f\n', num_dimensions, 1 - incorrect / size(validation_data, 1));
end

plot(dimensions_to_test, square_error);

