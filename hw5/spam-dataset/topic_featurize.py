import scipy.io
from algorithms import *
from sys import argv
from os import listdir
from os.path import isfile, join
import numpy as np
import re

def main():

    NUM_TEST_EXAMPLES = 5857
    test_filenames = [str(x) + '.txt' for x in range(NUM_TEST_EXAMPLES)]


    try:
        n_topics = int(argv[1])
        n_top_words = 40
        if n_topics <= 0 or n_top_words <= 0:
            print "Args error"
            exit(-1)
    except Exception:
        print "Args error"
        exit(-1)

    spam_mails = read_dir("spam/")
    ham_mails = read_dir("ham/")
    test_mails = read_file_names("test/", test_filenames)

    spam_size = len(spam_mails)
    ham_size = len(ham_mails)
    test_size = NUM_TEST_EXAMPLES

    all_mails = spam_mails + ham_mails + test_mails

    for i in xrange(len(all_mails)):
        all_mails[i] = re.sub(r'[0-9]+', ' ', all_mails[i])


    design_matrix, topics = generate_design_matrix(all_mails, n_topics, n_top_words)
    design_matrix = np.around(design_matrix * 100)

    training_data = design_matrix[0:spam_size + ham_size, :]
    test_data = design_matrix[spam_size + ham_size:, :]

    training_labels = np.vstack([np.ones((spam_size, 1)), np.zeros((ham_size, 1))])

    d = {"training_data" : training_data, "test_data" : test_data, "training_labels" : training_labels}
    scipy.io.savemat('spam_data_topic.mat', d)

    f = open("topics.txt", "w")
    for i in xrange(len(topics)):
        f.write("Topic {}:\n".format(i))
        for word in topics[i]:
            f.write("{} ".format(word))
        f.write("\n\n")

    f.close()











def generate_design_matrix(text_list, n_topics, n_top_words, n_features = 2000, max_df=0.95, min_df=2):
    text_matrix, feature_names = tfidf_vectorize(text_list, 2000, max_df, min_df)
    return extract_topics(text_matrix, feature_names, n_topics, n_top_words)

def read_dir(dir_path):
    text_list = []
    for file_name in listdir(dir_path):
        path = join(dir_path, file_name)
        if path.endswith(".txt") and isfile(path):
            f = open(path, "r")
            text_list.append(re.sub(r'[^ -~]+', ' ', f.read()).encode('ascii', 'ignore'))
            f.close()
    return text_list

def read_file_names(dir, file_names):
    l = []
    for file_name in file_names:
        f = open(join(dir, file_name))
        l.append(re.sub(r'[^ -~]+', ' ', f.read()).encode('ascii', 'ignore'))
        f.close
    return l



if __name__ == "__main__":
    main()

