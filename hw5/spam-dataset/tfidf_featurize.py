import scipy.io
from algorithms import *
from sys import argv
from os import listdir
from os.path import isfile, join
import numpy as np
import re

def main():

   

    spam_mails = read_dir("spam/")
    ham_mails = read_dir("ham/")
    test_mails = read_dir("test/")

    spam_size = len(spam_mails)
    ham_size = len(ham_mails)
    test_size = len(test_mails)

    all_mails = spam_mails + ham_mails + test_mails

    for i in xrange(len(all_mails)):
        all_mails[i] = re.sub(r'[0-9]+', ' ', all_mails[i])

    design_matrix, feature_names = tfidf_vectorize(all_mails, 2000)
    design_matrix = np.around(design_matrix * 100)

    training_data = design_matrix[0:spam_size + ham_size, :]
    test_data = design_matrix[spam_size + ham_size:, :]

    training_labels = np.vstack([np.ones((spam_size, 1)), np.zeros((ham_size, 1))])

    d = {"training_data" : training_data, "test_data" : test_data, "training_labels" : training_labels}
    scipy.io.savemat('spam_data_tfidf.mat', d)


    f = open("tfidf_feature_names.txt", "w")
    for i in xrange(len(feature_names)):
        f.write("{}:  {}\n".format(i, feature_names[i]))
    f.close()





def read_dir(dir_path):
    text_list = []
    for file_name in listdir(dir_path):
        path = join(dir_path, file_name)
        if path.endswith(".txt") and isfile(path):
            f = open(path, "r")
            text_list.append(re.sub(r'[^ -~]+', ' ', f.read()).encode('ascii', 'ignore'))
            f.close()
    return text_list



if __name__ == "__main__":
    main()

