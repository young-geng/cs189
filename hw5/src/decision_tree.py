from __future__ import division
import numpy as np
from scipy.io import savemat, loadmat
import scipy
from scipy.stats import itemfreq
from multiprocessing import Pool
import pickle


split_features = []


class TreeNode:
    left = None                 # Left child
    right = None                # Right child
    threshold = 1               # Decision threshold
    decision_boundaries = [0, 1, 2, 3]
    decision = None             # The final decision
    feature = None
    entropy_threshold = 0.05     # Ending condition for entropy
    level = None                # Depth of tree
    max_level = 10              # Maximum height
    entropy_gain_threshold = 0.001


    def __init__(self, X, Y, level=1, random_subset_rate=1):
        self.level = level
        self.grow_tree(X, Y, random_subset_rate)

    """ Compute the empirical entropy, only label is needed """
    def entropy(self, Y):
        s = 0
        l = Y.shape[0]
        for v in itemfreq(Y):
            s += v[1] / l * np.log(v[1] / l)
        return -s


    """ Compute the condition of entropy w.r.t feature number k """
    def conditional_entropy(self, X, Y, k):
        bins = itemfreq(X[:, k])
        s = 0
        for b in bins:
            v = b[0]
            c = b[1]
            t = np.vstack([Y[i, :] for i in xrange(X.shape[0]) if X[i, k] == v])
            s += c / X.shape[0] * self.entropy(t)
        return s

    """ Compute the entropy gain of the kth feature """
    def entropy_gain(self, X, Y, k):
        return self.entropy(Y) - self.conditional_entropy(X, Y, k)



    def set_max_as_decision(self, Y):
        d = None
        m = 0
        for v in np.unique(Y):
            if np.sum(Y == v) >= m:
                m = np.sum(Y == v)
                d = v
        self.decision = d
        return


    def split_samples(self, X, Y, k, split_policy):
        X_left = np.vstack([X[i, :] for i in xrange(X.shape[0]) if split_policy(X[i, k])])
        Y_left = np.vstack([Y[i, :] for i in xrange(X.shape[0]) if split_policy(X[i, k])])
        X_right = np.vstack([X[i, :] for i in xrange(X.shape[0]) if not split_policy(X[i, k])])
        Y_right = np.vstack([Y[i, :] for i in xrange(X.shape[0]) if not split_policy(X[i, k])])
        return X_left, Y_left, X_right, Y_right


    def select_feature(self, X, Y, random_subset_rate=1):
        feature_no = 0
        max_entropy_gain = 0
        threshold = 0
        feature_subset = range(X.shape[1])
        np.random.shuffle(feature_subset)
        feature_subset = feature_subset[0:int(len(feature_subset) * random_subset_rate)]
        for i in feature_subset:
            for b in self.decision_boundaries:
                if np.sum(X[:, i] <= b) == 0 or np.sum(X[:, i] <= b) == Y.shape[0]:
                    continue
                X_left, Y_left, X_right, Y_right = self.split_samples(X, Y, i, lambda x: x <= b)
                t = self.entropy(Y) - self.entropy(Y_left) * Y_left.shape[0] / Y.shape[0] - self.entropy(Y_right) * Y_right.shape[0] / Y.shape[0]
                if t >= max_entropy_gain:
                    feature_no = i
                    max_entropy_gain = t
                    threshold = b

        return feature_no, threshold, max_entropy_gain

    def grow_tree(self, X, Y, random_subset_rate=1):
        print "level: ", self.level
        """ We only have 1 class, make this node a leaf node """
        if len(np.unique(Y)) == 1:
            print "One class condition reached"
            self.decision = np.unique(Y)[0]
            return

        """ Ending condition reached """
        if self.entropy(Y) < self.entropy_threshold or self.level > self.max_level:
            print "entropy threshold reached!"
            self.set_max_as_decision(Y)
            return


        """ Select feature to split """

        self.feature, self.threshold, max_entropy_gain = self.select_feature(X, Y, random_subset_rate)
        if self.level == 1:
            #split_features.append((self.feature, self.threshold))
            pass

        if max_entropy_gain < self.entropy_gain_threshold:
            print "entropy gain threshold reached!"
            self.set_max_as_decision(Y)
            return

        """ if one side is empty """
        if np.sum(X[:,self.feature] <= self.threshold) == X.shape[0] or np.sum(X[:,self.feature] <= self.threshold) == 0:
            print "One side empty"
            d = None
            m = 0
            for v in np.unique(Y):
                if np.sum(Y == v) >= m:
                    m = np.sum(Y == v)
                    d = v
            self.decision = d
            return

        """ Spliting training set """
        split_policy = lambda x: x <= self.threshold
        X_left, Y_left, X_right, Y_right = self.split_samples(X, Y, self.feature, split_policy)

        self.left = TreeNode(X_left, Y_left, self.level + 1, random_subset_rate)
        self.right = TreeNode(X_right, Y_right, self.level + 1, random_subset_rate)


    def classify(self, x, verbose=False):
        if self.decision != None:
            if verbose:
                print "Leaf node reached, decision {}".format(self.decision)
            return self.decision

        if x[self.feature] <= self.threshold:
            if verbose:
                print "topic {} <= {}, going left".format(self.feature, self.threshold)
            return self.left.classify(x, verbose)
        
        if verbose:
            print "topic {} > {}, going right".format(self.feature, self.threshold)
        return self.right.classify(x, verbose)

        



def vote(results):
    if sum(results) / len(results) < 0.5:
        return 0
    return 1


def classify(trees, example, verbose=False):
    return vote(map(lambda tree: tree.classify(example, verbose), trees))



if __name__ == "__main__":
    d = loadmat("../spam-dataset/spam_data_topic.mat")
    train_X = d["training_data"]
    train_Y = d["training_labels"]
    test_X = d["test_data"]

    t = np.hstack([train_Y, train_X])

    np.random.shuffle(t)

    train_X = t[:5000, 1:]
    train_Y = t[:5000, 0:1]


    validation_X = t[5000:, 1:]
    validation_Y = t[5000:, 0:1]


    trees = []
    for i in xrange(40):
        trees.append(TreeNode(train_X, train_Y, random_subset_rate = 0.75))
 
    prediction = []
    for i in xrange(validation_Y.shape[0]):
        prediction.append(classify(trees, validation_X[i, :]))

    prediction = np.vstack(prediction)
    print sum(prediction)


    print sum(prediction == validation_Y) / validation_Y.shape[0]



    prediction = []
    for i in xrange(test_X.shape[0]):
        prediction.append(classify(trees, test_X[i, :]))

    prediction = np.vstack(prediction)

    f = open("kaggle.csv", "w")
    f.write("Id,Category\n")
    idx = 1
    for line in prediction:
        f.write("{},{}\n".format(idx, int(line[0])))
        idx += 1
    f.close()


    #pickle.dump(split_features, open("split_features.pickle", 'w'))
