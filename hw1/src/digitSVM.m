% SVM training and plotting for digit recognition


clear;

% Load data and reshape it into vectors
load '../data/digit-dataset/train.mat'
train_size = size(train_labels, 1);
train_x = [];
for idx = 1:train_size
    if mod(idx, 1000) == 0
        disp(idx);
    end
    train_x = [train_x; extractHOGFeatures(train_images(:, :, idx))];
end
train_x = double(train_x);

% train_x = reshape(train_images, size(train_images, 1) * size(train_images, 2), size(train_images, 3))';
disp(size(train_x));


% Randomize the training data
rand_order = randperm(train_size);
train_x = train_x(rand_order, :);
train_labels = train_labels(rand_order, :);


% Extract validation set
test_x = train_x(train_size - 10000 + 1: train_size, :);
test_y = train_labels(train_size - 10000 + 1: train_size, :);


% Problem 1
sample_size = [100, 200, 350, 500, 1000, 2000, 5000, 10000];
error_rate = [];
idx = 1;
for s = sample_size
    x = train_x(1:s, :);
    y = train_labels(1:s, :);
    model = train(y, sparse(x), '-s 2 -q -c 100');
    [predicted_y, accuracy, decision_value] = predict(test_y, sparse(test_x), model);
    error_rate = [error_rate, 1 - accuracy(1) / 100];
    [conf_mtx, order] = confusionmat(test_y, predicted_y);
    subplot(2, 4, idx);
    imagesc(conf_mtx);
    title(sprintf('Training set size: %d', sample_size(idx)));
    idx = idx + 1;
end

fprintf('%s\n', 'Press any key to continue');
pause;


fprintf('\n\n%s\n', 'Now printing and ploting error rate');

disp(error_rate);
plot(sample_size, error_rate);


fprintf('%s\n', 'Press any key to continue');
pause;





% Problem 3
% Cross-validation
small_train_size = 10000;
small_train_x = train_x(1:small_train_size, :);
small_train_y = train_labels(1:small_train_size, :);
indices = randi(10, small_train_size, 1);
error_rates = [];
c_range = 10 .^ double([-9:0.1:3]);
for c = c_range
    fprintf('%s %E\n', 'Training C = ', c);
    error_rate = 0;
    for idx = [1:10]
        validate_i = indices == idx;
        train_i = ~validate_i;
        x = small_train_x(train_i, :);
        y = small_train_y(train_i, :);
        validate_x = small_train_x(validate_i, :);
        validate_y = small_train_y(validate_i, :);
        model = train(y, sparse(x), sprintf('-s 2 -q -c %E', c));
        [predicted_y, accuracy, decision_value] = predict(validate_y, sparse(validate_x), model);
        error_rate = error_rate + 1 - accuracy(1) / 100;
    end
    error_rates = [error_rates, error_rate / 10];
end
disp(error_rates);
semilogx(c_range, error_rates);
best_c = c_range(find(error_rates == min(error_rates)));
fprintf('Best C = %f\n', best_c);
fprintf('%s\n\n\n', 'Press any key to continue');
pause;

%best_c = 1.995262;

fprintf('Now classifying kaggle data set\n');
model = train(train_labels, sparse(train_x), sprintf('-s 2 -q -c %E', best_c));
load('../data/digit-dataset/test.mat');
test_size = size(test_images, 3);
test_x = [];
for idx = 1:test_size
    test_x = [test_x; extractHOGFeatures(test_images(:, :, idx))];
end
test_x = double(test_x);
%test_x = reshape(test_images, size(test_images, 1) * size(test_images, 2), size(test_images, 3))';
[predicted_y, accuracy, decision_value] = predict(zeros(test_size, 1), sparse(test_x), model);
indices = [1:test_size]';
predicted_y = [indices, predicted_y];


FILE = fopen('kaggle_digit.csv', 'w');
fprintf(FILE, 'Id,Category\n');
fclose(FILE);

dlmwrite('kaggle_digit.csv', predicted_y, '-append');






