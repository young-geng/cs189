% Spam classification SVM

clear;

load '../data/spam-dataset/spam_data.mat';
train_x = double(training_data);
train_y = double(training_labels);
train_size = size(train_x, 1);
test_x = double(test_data);




% Randomize data
rand_order = randperm(train_size);
train_x = train_x(rand_order, :);
train_y = train_y(rand_order, :);



% Choose C
small_train_size = train_size;
small_train_x = train_x(1:small_train_size, :);
small_train_y = train_y(1:small_train_size, :);
indices = randi(10, small_train_size, 1);
error_rates = [];
c_range = 10 .^ [-8:1:8];
for c = c_range
    fprintf('%s %E\n', 'Training C = ', c);
    error_rate = 0;
    for idx = [1:10]
        validate_i = indices == idx;
        train_i = ~validate_i;
        x = small_train_x(train_i, :);
        y = small_train_y(train_i, :);
        validate_x = small_train_x(validate_i, :);
        validate_y = small_train_y(validate_i, :);
        model = train(y, sparse(x), sprintf('-s 2 -q -c %E', c));
        [predicted_y, accuracy, decision_value] = predict(validate_y, sparse(validate_x), model);
        error_rate = error_rate + 1 - accuracy(1) / 100;
    end
    error_rates = [error_rates, error_rate / 10];
end
semilogx(c_range, error_rates);
best_c = c_range(find(error_rates == min(error_rates)));
best_c = best_c(1);
fprintf('Best C = %E\n', best_c);
fprintf('%s\n', 'Press any key to continue');
pause;


% Report result
test_x = double(test_data);
test_size = size(test_data, 1);
model = train(train_y, sparse(train_x), sprintf('-s 2 -q -c %E', best_c));
[predicted_y, accuracy, decision_value] = predict(zeros(test_size, 1), sparse(test_x), model);

indices = [1:test_size]';
predicted_y = [indices, predicted_y];


FILE = fopen('kaggle_spam.csv', 'w');
fprintf(FILE, 'Id,Category\n');
fclose(FILE);

dlmwrite('kaggle_spam.csv', predicted_y, '-append');

