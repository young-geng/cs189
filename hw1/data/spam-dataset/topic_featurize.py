import pickle
import numpy as np
import scipy
import scipy.io
import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF
from nltk import wordpunct_tokenize
import os

def read_emails():
    file_name = sorted(os.listdir("./test"), key=lambda s: int(s.rstrip(".txt")))
    test = []

    for name in file_name:
        f = open("./test/{}".format(name))
        test.append(f.read())
        f.close()


    file_name = os.listdir("./ham")
    ham = []
    for name in file_name:
        f = open("./ham/{}".format(name))
        ham.append(f.read())
        f.close()

    file_name = os.listdir("./spam")
    spam = []
    for name in file_name:
        f = open("./spam/{}".format(name))
        spam.append(f.read())
        f.close()

    return ham, spam, test

def get_topics(documents):

    n_features = 1000
    n_topics = 20
    n_words = 200

    """ Vectorize """
    vectorizer = TfidfVectorizer(max_df=0.95, min_df=2, max_features=n_features, stop_words='english')
    vectors = vectorizer.fit_transform(documents)


    """ Non-negative matrix factorization """
    matrix = NMF(n_components=n_topics, random_state=1).fit(vectors)

    features = vectorizer.get_feature_names()
    topics = []

    for idx, topic in enumerate(matrix.components_):
        topics.append([features[i] for i in topic.argsort()[:-n_words - 1:-1]])

    return topics



def clean_up(documents):
    return map(lambda s: re.sub(r'[^ -~]+', ' ', s).encode('ascii', 'ignore'), documents)

def generate_vector(email, topics):
    v = np.zeros((1, len(topics)))
    for i in xrange(len(topics)):
        for word in topics[i]:
            v[0, i] += email.count(word)
    return v


def topic_featurize():
    ham, spam, test = read_emails()
    ham = clean_up(ham)
    spam = clean_up(spam)
    test = clean_up(test)

    topics = get_topics(spam + ham + test)

    train = ham + spam
    vectors = []
    for email in train:
        v = np.zeros((1, len(topics)))
        for i in xrange(len(topics)):
            for word in topics[i]:
                v[0, i] += email.count(word)
        vectors.append(v)

    X = np.vstack(vectors)
    Y = np.vstack([np.zeros((len(ham), 1)), np.ones((len(spam), 1))])

    train = test
    vectors = []
    for email in train:
        v = np.zeros((1, len(topics)))
        for i in xrange(len(topics)):
            for word in topics[i]:
                v[0, i] += email.count(word)
        vectors.append(v)


    test_design_matrix = np.vstack(vectors)
    return X, Y, test_design_matrix

if __name__ == "__main__":

    X, Y, test_design_matrix = topic_featurize()
    file_dict = {}
    file_dict['training_data'] = X
    file_dict['training_labels'] = Y
    file_dict['test_data'] = test_design_matrix
    scipy.io.savemat('spam_data_2.mat', file_dict)